﻿using UnityEngine;
using UnitySampleAssets.CrossPlatformInput;
 

namespace CompleteProject
{
    public class PlayerSelection : MonoBehaviour
    {
        public bool IsSelected; // If true, this player is selected
        public GameObject buttons; // Buttons to display on / off
        public Renderer rend; // Renderer with the selection plane
        public Renderer targetPoint; // Renderer with the target plane
        
        Animator anim; // Animation for the player


        void Start()
        {
            IsSelected = false;

            // Set up references.
			anim = GetComponent<Animator>();
			targetPoint = GameObject.Find("targetPoint").GetComponent<Renderer>();
			buttons = GameObject.Find("MenuRightClick");
        }

        void Update()
        {
            // Display appropriate renderers for selection
            if (IsSelected)
            {
                rend.enabled = true;
                anim.SetBool("IsSelected", true);
                GetComponent<PlayerMovement>().stayHere = true;
            }
            else
            {
                rend.enabled = false;
                anim.SetBool("IsSelected", false);
                GetComponent<PlayerMovement>().stayHere = false;
            }
        }




/****************************************************************/
        // Click on infest button
        public void infestClick()
        {
            buttons.GetComponent<CanvasGroup>().alpha = 0f;
            RightClick script = buttons.GetComponent<RightClick>();
            script.isShowing = false;
            targetPoint.enabled = false;

            if (IsSelected)
            {
                IsSelected = false;
                anim.SetBool("IsSelected", false);
                PlayerMovement scriptMov = GetComponent<PlayerMovement>();
                scriptMov.hasAgoal = true;
                scriptMov.stayHere = false;
                scriptMov.goal = script.targetPos3D;
                GetComponent<PlayerAttack>().infestMode = true;
                GetComponent<PlayerAttack>().eatMode = false;
            }
        }

        // Click on the eat button
        public void eatClick()
        {
            buttons.GetComponent<CanvasGroup>().alpha = 0f;
            RightClick script = buttons.GetComponent<RightClick>();
            script.isShowing = false;
            targetPoint.enabled = false;

            if (IsSelected)
            {
                IsSelected = false;
                anim.SetBool("IsSelected", false);
                PlayerMovement scriptMov = GetComponent<PlayerMovement>();
                scriptMov.hasAgoal = true;
                scriptMov.stayHere = false;
                scriptMov.goal = script.targetPos3D;
                GetComponent<PlayerAttack>().infestMode = false;
                GetComponent<PlayerAttack>().eatMode = true;
            }
        }
    }
}