﻿using UnityEngine;
using System.Collections;

namespace CompleteProject
{
    public class PlayerAttack : MonoBehaviour
    {
        public bool hasToAttack;
        public GameObject enemy;
        public float timeBetweenAttacks = 1f; // The time in seconds between each attack.
        public int attackDamage = 10; // The amount of health taken away per attack.

        public bool eatMode; // If true, is on eat mode
        public bool infestMode; // If true, is on infest mode

        Animator anim; // Reference to the animator component
        PlayerMovement mov; // Script dealing with player movement
		float timer; // Timer for counting up to the next attack.

        // Use this for initialization
        void Start()
        {
            hasToAttack = false;
            eatMode = true;
            infestMode = false;
            anim = GetComponent<Animator>();
            mov = GetComponent<PlayerMovement>();
			timer = 0f;
        }

        // Update is called once per frame
        void Update()
        {
            // Add the time since Update was last called to the timer.
            timer += Time.deltaTime;

            // If the timer exceeds the time between attacks, the player is in range and this enemy is alive...
            if (timer >= timeBetweenAttacks && hasToAttack && enemy.GetComponent<EnemyHealth> ().currentHealth > 0) {
				// ... attack.
				Attack ();
			} else {
				anim.SetBool ("Hurt", false);
				mov.stayHere = false;
			}

        }

        public void Attack()
        {
            //Reset the timer.
            timer = 0f;

            // If the player has health to lose...
            if (enemy.GetComponent<EnemyHealth> ().currentHealth > 0) {
				// ... damage the player.
				anim.SetBool ("Hurt", true);
				mov.stayHere = true;
				enemy.GetComponent<EnemyHealth> ().TakeDamage (attackDamage);
				if (eatMode) {
					enemy.GetComponent<EnemyHealth> ().eaten = true;
					enemy.GetComponent<EnemyHealth> ().infested = false;
				} else {
					enemy.GetComponent<EnemyHealth> ().infested = true;
					enemy.GetComponent<EnemyHealth> ().eaten = false;
				}
			} else {
				mov.hasHumanGoal = false;
			}
        }
    }
}