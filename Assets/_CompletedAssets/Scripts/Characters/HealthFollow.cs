﻿using UnityEngine;
using System.Collections;

namespace CompleteProject
{
    public class HealthFollow : MonoBehaviour
    {
        public Transform target; // Target to follow

        void Start()
        {
             
        }

        void Update()
        {
            Vector3 wantedPos = Camera.main.WorldToScreenPoint(target.position);
            wantedPos.y += 30;

            transform.position = wantedPos;
        }
    }
}