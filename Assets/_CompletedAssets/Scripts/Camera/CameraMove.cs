﻿using UnityEngine;
using System.Collections;

namespace CompleteProject
{
	public class CameraMove : MonoBehaviour
	{
		public float smoothing = 5f; // The speed with which the camera will be following.
		public Camera camera; // The main camera
		
		
		Vector3 target = Vector3.zero; // The position that that camera will be following.
		Vector3 offset; // The initial offset from the target.
		
		
		void Start()
		{
			camera = GetComponent<Camera>();
			
			// Calculate the initial offset.
			offset = transform.position - target;
		}
		
		
		void FixedUpdate()
		{
			// Store the input axes.
			// Get horizontal and vertical inputs (left and right arrows)
			float trans_h = Input.GetAxisRaw("Horizontal");
			float trans_v = Input.GetAxisRaw("Vertical");
			// Get the wheel of the mouse
			float wheel = Input.GetAxis("Mouse ScrollWheel");
			// Get the rotation (horizontal and vertical) of the mouse
			float rot_h = Input.GetAxis("Mouse X");
			float rot_v = Input.GetAxis("Mouse Y");
			
			
			
			Target(trans_h, trans_v); // Update the target of the camera
			Zoom(wheel); // Zoom
			
			Move(); // Move the camera based on the target
			//Turn(rot_h, rot_v); // Turning the camera (commented because right click is already used by the eat / infest menu)
			
			CheckForSelection(); // Check if a player is selected
		}
		
		
		
		
		/****************************************************************/
		
		void Target(float h, float v)
		{
			// Set the movement vector based on the axis input.
			target.x += h;
			target.z += v;
		}
		
		void Zoom(float w)
		{
			// Set the zoom based on the wheel
			if((transform.position.y > 100 && w > 0) || (transform.position.y < 250 && w < 0))
				target.y -= w*30;
		}
		
		void Move()
		{
			// Create a postion the camera is aiming for based on the offset from the target.
			Vector3 targetCamPos = target + offset;
			
			// Smoothly interpolate between the camera's current position and it's target position.
			transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
		}
		
		void Turn(float h, float v)
		{
			// If the right click is pressed
			if (Input.GetMouseButton(1))
			{
				transform.RotateAround(target, Vector3.up, h * 2);
				transform.RotateAround(target, Vector3.right, v * 2);
			}
		}
		
		void CheckForSelection()
		{
			
			if (Input.GetMouseButtonUp(0))
			{
				Ray ray = camera.ScreenPointToRay(Input.mousePosition); // ray from camera to mouse
				
				RaycastHit hit = new RaycastHit(); // impact point
				
				if (Physics.Raycast(ray, out hit, 10000))
				{
					if (hit.collider.tag == "zombie") // If hit a zombie
					{
						// Switch its selected status
						PlayerSelection script = hit.collider.GetComponent<PlayerSelection>();
						if (script.IsSelected)
							script.IsSelected = false;
						else
							script.IsSelected = true;
						
						Debug.DrawLine(ray.origin, hit.point);
					}
				}
			}
		}
	}
}