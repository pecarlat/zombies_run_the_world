﻿using UnityEngine;
using System.Collections;

namespace CompleteProject
{
    public class EnemyLeak : MonoBehaviour {

	    // Use this for initialization
	    void Start () {
	
	    }
	
	    // Update is called once per frame
	    void Update () {
            Vector3 poses = new Vector3(0,0,0);
            EnemyDetection script = GetComponent<EnemyDetection>();
           
            if(script.zombiesInField.Count > 0) {
                for (int i = 0; i < script.zombiesInField.Count; i++ )
                {
                    poses[0] += script.zombiesInField[i].transform.position[0];
                    poses[1] += script.zombiesInField[i].transform.position[1];
                    poses[2] += script.zombiesInField[i].transform.position[2];
                }

                poses /= script.zombiesInField.Count;

                EnemyMovement movScr = GetComponent<EnemyMovement>();
                if(movScr.hasAgoal) {
                    movScr.goal = ((transform.position - poses)*5 + movScr.goal)/2f;
                } else {
                    movScr.goal = (transform.position - poses)*5;
                }
                movScr.hasAgoal = true;
                movScr.stayHere = false;
            }
	    }
    }
}
