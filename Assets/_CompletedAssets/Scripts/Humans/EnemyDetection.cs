﻿using UnityEngine;
using System.Collections;
using UnitySampleAssets.CrossPlatformInput;
using System.Collections.Generic;

namespace CompleteProject
{
    public class EnemyDetection : MonoBehaviour
    {
        public int fieldOfView = 100;
        public int fieldOfHear = 360;
        public int step = 5;
        public float view = 75f;
        public float hear = 20f;

        public List<GameObject> zombiesInField;

        Quaternion rotation;
        Quaternion startingAngleView;
        Quaternion startingAngleHear;
        Quaternion stepAngle;
		Rigidbody playerRigidbody;

		Animator anim; // Reference to the animator component
		EnemyMovement mov; // Script dealing with player movement
		EnemyAttack att; // Script dealing with player's attacks

        // Use this for initialization
        void Start()
        {
            startingAngleView = Quaternion.AngleAxis(-fieldOfView / 2f, Vector3.up);
            startingAngleHear = Quaternion.AngleAxis(-fieldOfHear / 2f, Vector3.up);
            stepAngle = Quaternion.AngleAxis(step, Vector3.up);
			playerRigidbody = GetComponent<Rigidbody>();
			anim = GetComponent<Animator>();
			mov = GetComponent<EnemyMovement> ();
			att = GetComponent<EnemyAttack>();
        }

        // Update is called once per frame
        void Update()
        {
            RaycastHit hit = new RaycastHit();
            Vector3 pos = transform.position;
            pos[1] += 5;
            Detect(hit, pos, fieldOfView, view, startingAngleView);
            Detect(hit, pos, fieldOfHear, hear, startingAngleHear);
        }

        void Detect(RaycastHit hit, Vector3 pos, int field, float length, Quaternion startingAngle)
        {
            zombiesInField.Clear();
            Quaternion angle = transform.rotation * startingAngle;
            Vector3 direction = angle * Vector3.forward;
            for (var i = 0; i < field / step; i++)
            {
                if (Physics.Raycast(pos, direction, out hit, length))
                {
                    if (hit.collider.tag == "zombie")
                    {
						print (Avancement.humainsVivants);
						
						if (Avancement.humainsVivants < 5)
						{

							if (GetComponent<EnemyHealth>().currentHealth <= 50)
							{
								zombiesInField.Add(hit.rigidbody.gameObject);
								rotation = startingAngle * Quaternion.Euler(0, 180, 0);
								mov.hasAgoal = true;
								
								playerRigidbody.MoveRotation(rotation);
								Debug.DrawLine(pos, hit.point); //test pour debug le rayon

							}
							if (Vector3.Distance(pos, hit.collider.attachedRigidbody.position) > 8.0)
							{
								anim.SetBool("Hurt", false);
								att.hasToAttack = false;
								
								mov.hasAgoal = true;
								mov.goal = hit.point;
								if (!anim.GetBool("IsRunning"))
								{
									anim.SetBool("IsRunning", true);
								}
							}
							else
							{
								if (anim.GetBool("Hurt") == false)
								{
									att.player = hit.transform.gameObject;
									att.hasToAttack = true;
									//att.Attack(hit.collider);
								}
							}
						}
						
						else if(Avancement.humainsVivants < 5)
						{
							zombiesInField.Add(hit.rigidbody.gameObject);
							rotation = startingAngle * Quaternion.Euler(0, 180, 0);
							mov.hasAgoal = true;
							
							playerRigidbody.MoveRotation(rotation);
							Debug.DrawLine(pos, hit.point); //test pour debug le rayon
						}
                    }
                    else if (hit.collider.tag == "human")
                    {
                        //Debug.DrawLine(pos, hit.point); //test pour debug le rayon
                        //print("ennemy");

                    }

                }
                direction = stepAngle * direction;
            }
        }
    }
}