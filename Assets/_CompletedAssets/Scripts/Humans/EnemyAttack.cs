﻿using UnityEngine;
using System.Collections;

namespace CompleteProject
{
	public class EnemyAttack : MonoBehaviour
	{
		public bool hasToAttack;
		public GameObject player;
		public float timeBetweenAttacks = 1f; // The time in seconds between each attack.
		public int attackDamage = 5; // The amount of health taken away per attack.
		
		Animator anim; // Reference to the animator component
		EnemyMovement mov; // Script dealing with player movement
		float timer; // Timer for counting up to the next attack.
		PlayerHealth pHealth;
		
		// Use this for initialization
		void Start()
		{
			hasToAttack = false;
			anim = GetComponent<Animator>();
			mov = GetComponent<EnemyMovement>();
			timer = 0f;
		}
		
		// Update is called once per frame
		void Update()
		{
			// Add the time since Update was last called to the timer.
			timer += Time.deltaTime;
			
			// If the timer exceeds the time between attacks, the player is in range and this enemy is alive...
			if (timer >= timeBetweenAttacks && hasToAttack && player.GetComponent<PlayerHealth> ().currentHealth > 0) {
				// ... attack.
				Attack ();
			} else {
				anim.SetBool ("Hurt", false);
				mov.stayHere = false;
			}
			
		}
		
		public void Attack()
		{
			//Reset the timer.
			timer = 0f;
			
			// If the player has health to lose...
			if (player.GetComponent<PlayerHealth> ().currentHealth > 0) {
				// ... damage the player.
				anim.SetBool ("Hurt", true);
				mov.stayHere = true;
				player.GetComponent<PlayerHealth> ().TakeDamage (attackDamage);
			} else {
				mov.hasAgoal = false;
			}
		}
	}
}