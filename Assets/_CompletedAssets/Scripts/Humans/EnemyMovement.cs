﻿using UnityEngine;
using System.Collections;

namespace CompleteProject
{
    public class EnemyMovement : MonoBehaviour
    {
        public float walkSpeed = 7f; // The speed that the player will move at
        public float runSpeed = 15f; // The speed that the player will run at

        public GameObject selection; // The plane to display if selected

        public bool hasAgoal; // If true, has to go somewhere
        public Vector3 goal; // The goal to reach
        public bool stayHere; // If true, has to stay where he is
        public bool randomWalk; // If true has to do a random walk

        public bool isWalking; // If true, has to walk
        public bool isRunning; // If true, has to run


        Transform player;               // Reference to the player's position.
        PlayerHealth playerHealth;      // Reference to the player's health.
        EnemyHealth enemyHealth;        // Reference to this enemy's health.
        UnityEngine.AI.NavMeshAgent nav;               // Reference to the nav mesh agent.

        Vector3 movement; // The vector to store the direction of the player's movement.
        Animator anim; // Reference to the animator component.

        static System.Random rnd = new System.Random(); // Random

        int maxTempo = 1000; // For the random walk, will change its direction all maxTempo iterations or less
        int currTempo = 100; // Tempo for random walk will vary in time
        int cpt = 0; // A global counter
        float h = 0; // Horizontal direction to follow
        float v = 0; // Vertical direction to follow

        void Awake()
        {
            // Set up references.
            anim = GetComponent<Animator>();

            hasAgoal = false;
            stayHere = false;
            randomWalk = true;
            isWalking = false;
            isRunning = false;
        }


        void FixedUpdate()
        {
            // Update the direction to follow according to its status
            UpdateDirection();

            // Move the player around the scene.
            Move(h, v);

            // Animate the player.
            Animating(h, v);

            // Check if the goal is reached
            if (hasAgoal)
                CheckGoal();
        }




        /****************************************************************/

        void UpdateDirection()
        {
            // Random walk
            if (randomWalk)
            {
                if (cpt == currTempo)
                {
                    h = rnd.Next(3) - 1;
                    v = rnd.Next(3) - 1;
                    cpt = 0;
                    currTempo = rnd.Next(10, maxTempo + 1);
                }
            }
            // Follow goal
            if (hasAgoal)
            {
                h = goal[0] - transform.position[0];
                v = goal[2] - transform.position[2];
            }
            // Do nothing
            if (stayHere)
            {
                h = 0;
                v = 0;
                cpt++;
                if (cpt > 200)
                {
                    cpt = 0;
                    stayHere = false;
                }
            }
            cpt++;

            // Update behavioral comportments
            // Boolean that is true if either of the input axes is non-zero
            bool walking = h != 0f || v != 0f;

            // Tell the animator whether or not the player is walking.
            if (!hasAgoal)
            {
                isWalking = walking;
            }
            else
            {
                isWalking = true;
                isRunning = true;
            }
        }

        void Move(float h, float v)
        {
            // Set the movement vector based on the axis input.
            movement.Set(h, 0f, v);

            // Normalise the movement vector and make it proportional to the speed per second.
            float speed = isRunning ? runSpeed : walkSpeed;
            movement = movement.normalized * speed * Time.deltaTime;

            if (movement != Vector3.zero)
            {
                Quaternion rotate = Quaternion.LookRotation(movement);
                // Set the player's rotation to this new rotation.
                GetComponent<Rigidbody>().MoveRotation(rotate);
            }

            // Move the player to it's current position plus the movement.
            GetComponent<Rigidbody>().MovePosition(transform.position + movement);
        }

        void Animating(float h, float v)
        {
            // Chose appropriate animation
            anim.SetBool("IsWalking", isWalking);
            anim.SetBool("IsRunning", isRunning);
        }

        void CheckGoal()
        {
            Vector3 curr = goal - transform.position;
            if (curr[0] < 5f && curr[0] > -5f && curr[2] < 5f && curr[2] > -5f)
            {
                hasAgoal = false;
                stayHere = true;
                h = 0;
                v = 0;
                isWalking = false;
                isRunning = false;
                cpt = 0;
            }
        }
    }
}