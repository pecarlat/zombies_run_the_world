﻿using UnityEngine;
using System.Collections;

namespace CompleteProject
{
	public class RightClick : MonoBehaviour
	{
		public Camera camera; // Camera to use for the raycast
		public GameObject buttInfest; // Button for infest
		public GameObject buttEat; // Button for eat
		public Renderer targetPoint; // Plane displaying where we are pointing on
		public bool isShowing; // Display or not the target
		
		public Vector3 targetPos3D; // Position of the target
		
		void Start()
		{
			isShowing = false;
			targetPoint.enabled = false;
		}
		
		void Update()
		{
			WaitForRightClick();
		}
		
		
		
		
		/****************************************************************/
		
		void WaitForRightClick()
		{
			// If right click
			if (Input.GetMouseButtonDown(1))
			{
				// If have to display the menu and target
				if (!isShowing)
				{
					Ray ray = camera.ScreenPointToRay(Input.mousePosition); // ray from camera to mouse
					RaycastHit hit = new RaycastHit(); // impact point
					
					if (Physics.Raycast(ray, out hit, 10000))
					{
						// Get the target and display it
						targetPos3D = hit.point;
						targetPos3D[1] += 0.1f;
						targetPoint.transform.position = targetPos3D;
						targetPoint.enabled = true;
						
						// Display the menu
						Vector3 pos = Input.mousePosition;
						pos[1] += 50;
						buttInfest.transform.position = pos;
						pos[1] -= 25;
						buttEat.transform.position = pos;
						gameObject.GetComponent<CanvasGroup>().alpha = 1f;
					}
				}
				else
				{
					// Undisplay targetpoint and menu
					targetPoint.enabled = false;
					gameObject.GetComponent<CanvasGroup>().alpha = 0f;
				}
				isShowing = !isShowing;
			}
		}
		
		public void onEatClick()
		{
			GameObject[] allZombies = GameObject.FindGameObjectsWithTag("zombie");
			for (int i = 0; i < allZombies.Length; i++)
			{
				print("here");
				allZombies[i].GetComponent<PlayerSelection>().eatClick();
			}
		}
		
		public void onInfestClick()
		{
			GameObject[] allZombies = GameObject.FindGameObjectsWithTag("zombie");
			print("infest zombie " + allZombies.Length);
			for (int i = 0; i < allZombies.Length; i++)
			{
				allZombies[i].GetComponent<PlayerSelection>().infestClick();
			}
		}
	}
}