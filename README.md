# Zombies run the world
School project made to embed some AI behaviors into humans and zombies evolving in a simulation of our university. As a disclaimer, the project was made in a very short amount of time, and I had many other projects to do in parallel and, anyway, it is not quite finished, but still fun to watch. The demo is here to enlighten how the bad AI works haha.

## Requirements
You will need Unity 5.6, and 5.5 should also be fully compatible.

## Demo

![Demo](resources/demo.gif)

## Notes
The Library must be removed from the gitignore, sill need to find out why.
